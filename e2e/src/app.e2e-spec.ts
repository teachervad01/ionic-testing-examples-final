import { AppPage } from './app.po';

describe('new App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should have content', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toContain('COVID-19');
  });

  it('should have a title', () => {
    expect(page.getAppTitle()).toEqual('My Great Application');
  })
});

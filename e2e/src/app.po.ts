import { browser, by, element, protractor } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.deepCss('app-root ion-content')).getText();
  }

  getTextFromElement(selector: string){
    return element(by.css(selector)).getText();
  }

  getTextFromDeepElement(slector: string){
    return element(by.deepCss(slector)).getText();
  }

  getAppTitle(){
    return browser.getTitle();
  }

  waitPageToLoad(page: string){
    browser.wait(protractor.ExpectedConditions.urlContains(page), 5000);
  }
}
